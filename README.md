# README #

In this game you are a male pigeon doing the courtship ritual to female pigeon. The keys F, G, H and J allow the pidgeon to do one ritual: move the wings, chirp, jump and move the tail. Every ritual can make the female pigeon move nearer or farther to the male pigeon.
 
There are three levels. For each level you have a number of seconds to try entice the female pigeon. If the counter gets to 0 the game is reset. In the two first levels there are a number of rituals that the female pigeon always like. But in the last level, the rituals will change the way they affect the female pigeon every three seconds. So, the female pigeon will not like the same ritual all the time.

### Executable ###

A Windows build can be found in the downloads section
