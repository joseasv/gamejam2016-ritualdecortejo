﻿using UnityEngine;
using System.Collections;

public class AccionesProta : MonoBehaviour {


	private Vector3 posFinalSalto;
	private Vector3 inicio;
	private float speed = 10.0f;
	private float journeyLength;
	private float startTime;

	// Use this for initialization
	void Start () {
		inicio = transform.position;
		posFinalSalto = inicio + new Vector3 (0, 0.8f, 0);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.F)) {
			print ("f presionada");


			transform.position = inicio;
			StartCoroutine (saltar ());


		}
	}

	IEnumerator saltar(){
		


		while (transform.position != posFinalSalto) {
			float paso = speed * Time.deltaTime;
			transform.position = Vector3.MoveTowards (transform.position, posFinalSalto, paso);
			yield return null;
		}
		transform.position = inicio;

		print("saltar terminado");

	}
}
