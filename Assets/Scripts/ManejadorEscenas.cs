﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using System;

public class ManejadorEscenas : MonoBehaviour
{

    private static ManejadorEscenas instancia;
    public GameObject prota;
    public GameObject hembra;
    public Vector3 posFinalHembra;
    private Dictionary<KeyCode, int> teclaAfecto = new Dictionary<KeyCode, int>();
    private Vector3 posInicioHembra;
    private Vector3 posProta;
    private Animator animProta;
    private bool teclasActivas;
    public Text tiempo;
    private int tiempoInt;
    private IEnumerator corutinaContador;

    // triggers de animacion del prota
    private int triggerSalto;
    private int triggerChirrido;
    private int triggerMoverCola;
    private int triggerAleteo;

    void Awake()
    {
        if (!instancia)
        {
            instancia = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this);
    }

    // Use this for initialization
    void Start()
    {
        // Inicializar posiciones de los pajaros

    }


    void OnLevelWasLoaded(int level)
    {
        print("iniciando la escena " + level);
        posProta = new Vector3(-6, 0.85f, 0);
        tiempo = GameObject.FindWithTag("Texto").GetComponent<Text>();
        print(GameObject.Find("Canvas").GetComponent<Text>());
        posFinalHembra = new Vector3(-4, 0.85f, 0);
        posInicioHembra = new Vector3(8, 0.85f, 0);

        triggerSalto = Animator.StringToHash("Salto");
        triggerChirrido = Animator.StringToHash("Chirrido");
        triggerMoverCola = Animator.StringToHash("MoverCola");
        triggerAleteo = Animator.StringToHash("Aleteo");

        print("inicio la escena " + level);

        prota = GameObject.Find("prota");
        prota.transform.position = posProta;
        animProta = prota.GetComponent<Animator>();


        hembra = GameObject.Find("hembra");
        hembra.transform.position = posInicioHembra;


        // Inicializando el afecto de las teclas segun el nivel. El nivel 3 cambia cada 3 segundos en el metodo Update
        int[] afectos = new int[0];
        
        switch (level)
        {
            case 1:
                tiempoInt = 20;
                afectos = new int[] { 10, 20, 20, -10 };
                break;

            case 2:
                tiempoInt = 15;
                afectos = new int[] { -10, 20, 20, -10 };
                break;

            case 3:
                tiempoInt = 8;
                afectos = new int[] { -10, 25, -20, -15 };
                StartCoroutine(cambiarTeclaAfecto());
                break;
        }
        corutinaContador = tiempoTexto();
        StartCoroutine(corutinaContador);
        inicializarTeclaAfecto(afectos);
        teclasActivas = true;
    }

    public IEnumerator tiempoTexto()
    {
        while (true)
        {
            tiempoInt--;
            tiempo.text = tiempoInt.ToString();


            yield return new WaitForSeconds(1);
        }
    }

    // Aleatoriamente asigna los valores del arreglo a las teclas F, G, H y J
    private void inicializarTeclaAfecto(int[] afectos)
    {
        teclaAfecto.Clear();
        KeyCode[] teclas = new KeyCode[] { KeyCode.F, KeyCode.G, KeyCode.H, KeyCode.J };
        List<int> indices = new List<int>() { 0, 1, 2, 3 };

        for (int i = 0; i < teclas.Length; i++)
        {

            int indice = indices[UnityEngine.Random.Range(0, indices.Count)];

            indices.Remove(indice);
            teclaAfecto.Add(teclas[i], afectos[indice]);
            print(i + " " + indice);
            print("tecla " + teclas[i] + " es " + afectos[indice]);
        }
    }



    // Update is called once per frame
    void Update()
    {
        chequearEscenaTerminada();

        if (teclasActivas)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                int afecto = teclaAfecto[KeyCode.F];

                animProta.SetTrigger(triggerSalto);
                hembra.GetComponent<AccionesHembra>().enamorar(afecto);
                //acercar hembra
            }
            else if (Input.GetKeyDown(KeyCode.G))
            {
                int afecto = teclaAfecto[KeyCode.G];
                animProta.SetTrigger(triggerAleteo);
                hembra.GetComponent<AccionesHembra>().enamorar(afecto);
                //acercar hembra
            }
            else if (Input.GetKeyDown(KeyCode.H))
            {
                int afecto = teclaAfecto[KeyCode.H];
                animProta.SetTrigger(triggerMoverCola);
                hembra.GetComponent<AccionesHembra>().enamorar(afecto);
                //acercar hembra
            }
            else if (Input.GetKeyDown(KeyCode.J))
            {
                int afecto = teclaAfecto[KeyCode.J];
                animProta.SetTrigger(triggerChirrido);
                hembra.GetComponent<AccionesHembra>().enamorar(afecto);
                //acercar hembra
            }
        }

    }

    // Si es la tercera escena los valores de las teclas cambiaran aleatoriamente cada 3 segundos
    private IEnumerator cambiarTeclaAfecto()
    {
        while (true)
        {
            int[] afectos = new int[] { -10, 20, -20, -10 };
            inicializarTeclaAfecto(afectos);

            yield return new WaitForSeconds(3f);
        }

    }

    private void chequearEscenaTerminada()
    {

        if (tiempoInt == 0)
        {
            SceneManager.LoadScene("escena0");
            Destroy(this.gameObject);
        }

        else if (hembra.gameObject.transform.position.x < posFinalHembra.x)
        {
            //corutina para terminar juego y al final cambia la escena
            print("escena terminada");
            teclasActivas = false;
            StopCoroutine(cambiarTeclaAfecto());
            
            StopCoroutine(corutinaContador);
            print("cambiando escena ");
            int escenaActual = SceneManager.GetActiveScene().buildIndex;
           
            switch (escenaActual)
            {
                case 1:
                    SceneManager.LoadScene("escena2");
                    break;
                case 2:
                    SceneManager.LoadScene("escena3");
                    break;
                case 3:
                    SceneManager.LoadScene("escena0");
                    Destroy(this.gameObject);
                    break;
            }
        }



    }

    private IEnumerator cambiarEscena()
    {
        print("cambiando escena ");
        int escenaActual = SceneManager.GetActiveScene().buildIndex;
        //animacion
        yield return new WaitForSeconds(3f);
        switch (escenaActual)
        {
            case 1:
                SceneManager.LoadScene("escena2");
                break;
            case 2:
                SceneManager.LoadScene("escena3");
                break;
            case 3:
                SceneManager.LoadScene("escena0");
                break;
        }
        yield return null;
    }
}
